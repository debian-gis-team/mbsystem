# add GMT binaries to the PATH if needed

GMTBIN="/usr/lib/gmt/bin"

if [ `echo "$PATH" | grep -c "$GMTBIN"` -eq 0 ] ; then
   PATH="$PATH:$GMTBIN"
fi
